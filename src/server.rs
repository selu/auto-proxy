use crate::pac::{self, Proxy};
use futures::future;
use futures::prelude::*;
use hyper::service::{service_fn, make_service_fn};
use hyper::{Body, Client, Method, Request, Response, Server};
use log::{debug, error, info};
use std::sync::{Arc, Mutex};
use std::convert::Infallible;
use std::net::SocketAddr;
use hyper::upgrade::Upgraded;

pub async fn start(pac: pac::PAC) -> () {
    let pac = Arc::new(Mutex::new(pac));
    let addr = ([0, 0, 0, 0], 8000).into();
    let make_svc = make_service_fn(move |_conn| {
        let pac = pac.clone();
        async move {
            Ok::<_, Infallible>(service_fn(move |req| proxy(req, pac.clone())))
        }
    });
    let server = Server::bind(&addr).serve(make_svc);

    info!("start server at: {}", addr);
    if let Err(e) = server.await {
        error!("server error: {}", e);
    }
}

async fn proxy(req: Request<Body>, pac: Arc<Mutex<pac::PAC>>) -> Result<Response<Body>, hyper::Error> {
    debug!("request: {:?}", req);
    let url = format!(
        "{}://{}",
        req.uri().scheme().unwrap_or(&http::uri::Scheme::HTTP),
        req.uri().host().unwrap(),
    );
    let proxies = pac
        .lock()
        .expect("no PAC")
        .call(&url, req.uri().host().unwrap())
        .expect("no proxies");
    let proxy = &proxies[0]; // get the first one
    debug!("proxy: {:?}", proxy);
    let res: Option<_> = match req.method() {
        // https
        &Method::CONNECT => match proxy {
            Proxy::Proxy(uri) => {
                debug!("https indirectly");
                let uri = format!(
                    "http://{}:{}",
                    uri.host().unwrap(),
                    uri.port_u16().unwrap_or(80)
                )
                .parse()
                .unwrap();
                let proxy = hyper_proxy::Proxy::new(hyper_proxy::Intercept::All, uri);
                let conn = hyper::client::HttpConnector::new();
                let client = Client::builder()
                    .build(hyper_proxy::ProxyConnector::from_proxy(conn, proxy).unwrap());
                let new_req = {
                    let mut builder = Request::connect(format!(
                        "http://{}:{}",
                        req.uri().host().unwrap(),
                        req.uri().port_u16().unwrap_or(443)
                    ));
                    let headers = builder.headers_mut().unwrap();
                    for (key, value) in req.headers().iter() {
                        headers.append(key, value.clone());
                    }
                    builder.body(Body::empty()).unwrap()
                };
                let res = client.request(new_req).await;
                match res {
                    Err(_) => None,
                    Ok(res) => {
                        if res.status() == 200 {
                            let upgrade_q = req
                                .into_body()
                                .on_upgrade();
                            let upgrade_s = res
                                .into_body()
                                .on_upgrade();
                            tokio::spawn(future::join(upgrade_q, upgrade_s).then(|(cli, ser)| async move {
                                let (mut crx, mut ctx) = tokio::io::split(cli.unwrap());
                                let (mut srx, mut stx) = tokio::io::split(ser.unwrap());
                                let amounts = future::try_join(tokio::io::copy(&mut crx, &mut stx), tokio::io::copy(&mut srx, &mut ctx)).await;
                                match amounts {
                                    Ok((from_client, from_server)) => debug!(
                                        "client wrote {} bytes and received {} bytes",
                                        from_client, from_server
                                    ),
                                    Err(e) => error!("tunnel error: {}", e)
                                }
                            }));
                            Some(Ok(Response::builder().status(200).body(Body::empty()).unwrap()))
                        } else {
                            Some(Ok(res))
                        }
                    }
                }
            }
            Proxy::Direct => {
                debug!("https directly");
                let ipaddrs = dns_lookup::lookup_host(req.uri().host().unwrap()).unwrap();
                let saddr =
                    std::net::SocketAddr::new(ipaddrs[0], req.uri().port_u16().unwrap_or(443));
                tokio::spawn(async move {
                    match req.into_body().on_upgrade().await {
                        Ok(upgraded) => {
                            if let Err(e) = tunnel_direct(upgraded, saddr).await {
                                error!("server io error: {}", e);
                            }
                        }
                        Err(e) => error!("upgrade error: {}", e)
                    }
                });

                Some(Ok(Response::new(Body::empty())))
            }
            _ => None,
        },
        // http
        _ => {
            match proxy {
                Proxy::Proxy(uri) => {
                    debug!("http indirectly");
                    let uri = format!(
                        "http://{}:{}",
                        uri.host().unwrap(),
                        uri.port_u16().unwrap_or(80)
                    )
                    .parse()
                    .unwrap();
                    let proxy = hyper_proxy::Proxy::new(hyper_proxy::Intercept::All, uri);
                    let conn = hyper::client::HttpConnector::new();
                    let client = Client::builder()
                        .build(hyper_proxy::ProxyConnector::from_proxy(conn, proxy).unwrap());
                    Some(client.request(req).await)
                }
                Proxy::Direct => {
                    debug!("http directly");
                    let client = Client::new();
                    Some(client.request(req).await)
                }
                _ => None, // others are not handled
            }
        }
    };
    match res {
        Some(res) => Ok(res.unwrap()),
        None => Ok(Response::builder()
            .status(500)
            .body(Body::empty()).unwrap()),
    }
}

async fn tunnel_direct(upgraded: Upgraded, addr: SocketAddr) -> std::io::Result<()> {
    let mut stream = tokio::net::TcpStream::connect(&addr).await?;

    let amounts = {
        let (mut crx, mut ctx) = tokio::io::split(upgraded);
        let (mut srx, mut stx) = stream.split();
        future::try_join(tokio::io::copy(&mut crx, &mut stx), tokio::io::copy(&mut srx, &mut ctx)).await
    };

    match amounts {
        Ok((from_client, from_server)) => debug!(
            "client wrote {} bytes and received {} bytes",
            from_client, from_server
        ),
        Err(e) => error!("tunnel error: {}", e)
    };
    Ok(())
}
