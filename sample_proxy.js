// test proxy config for local squid
function FindProxyForURL(url, host)
{
    normal = "DIRECT";
    proxies = "PROXY localhost:3128; DIRECT;";

    // Make everything lower case.
    url = url.toLowerCase();
    host = host.toLowerCase();
    if (isPlainHostName(host)) return normal;
    if (/.*\.hu$/.test(host)) return proxies;
    // bail out
    return normal;
};
